#Terraform Integration with Puppets



This repository shows how to combine Terraform for infrastructure provisioning with Puppet for configuration management. Terraform is used to automate the setup and configuration of infrastructure resources on AWS, and Puppet manifests are used to configure those resources.


## Contents Table

- [Introduction] (#introduction)
- [Requirements](#requirements) - [Setup](#setup)
  - [Status](#stability)
- [Management of Configuration](#configuration-management)
[Examining](#examining)
- [References](#references)
- [Problems](#problems)

## Introduction

The goal of this project is to use Terraform and Puppet to automate the configuration management of infrastructure resources that are provisioned on AWS. Making sure that infrastructure resources are provisioned and configured consistently and effectively is the primary goal.

## Configuration

### Prerequisites

Make sure you have the following installed before beginning this project:

- GitLab account
- Terraform 
- Puppet 
- AWS CLI

### Deployment

1. **Clone Repository**: Clone this repository to your local machine.

   ```bash
   git clone [repository_url]
   
2. **Terraform Setup**:

   - Update the provider block in `terraform.tf` with your AWS provider configuration.
   - Modify any other configuration variables in `terraform.tf` as needed.
   - Initialize Terraform:

     ```bash
     terraform init
   - Deploy the infrastructure using Terraform:

     ```bash
     terraform apply

3. **Setup of Puppets**:

   - Create Puppet manifests in the `puppet/` directory to configure your resources.
   - Make sure the expected state of your resources is accurately defined in your Puppet manifests.

### Management of Configurations

Puppet will automatically execute on the provided instances and configure them in accordance with the defined Puppet manifests following the infrastructure's deployment using Terraform. Make sure that your Puppet manifests correctly set up the resources that have been provisioned in the appropriate state.

### Examination

Verify that the resources that were provisioned adhere to the ideal configuration state as specified in the Puppet manifests.
- Carry out any required debugging and troubleshooting to make sure Terraform and Puppet are integrated properly.
