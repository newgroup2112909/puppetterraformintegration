provider "aws" {
  region = "us-east-1"  
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow-ssh"
  description = "Allow SSH inbound traffic"
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "server" {
  ami = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name = "main"
  security_groups = [aws_security_group.allow_ssh.name]

  metadata_options {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 1
  }

  provisioner "local-exec" {
    command = "puppet apply /home/master/puppet/site.pp"
  }
  
  user_data = <<-EOF
              #!/bin/bash
              
              sudo apt-get update
                
                wget https://apt.puppet.com/puppet6-release-bionic.deb
                sudo dpkg -i puppet6-release-bionic.deb
                sudo apt-get update && sudo apt-get install -y puppetserver
                
                sudo sed -i 's/-Xms2g/-Xms512m/g' /etc/default/puppetserver
                sudo sed -i 's/-Xmx2g/-Xmx512m/g' /etc/default/puppetserver
                
                sudo systemctl start puppetserver
                
                sudo systemctl enable puppetserver
              EOF
}
